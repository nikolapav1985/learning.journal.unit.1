#!/bin/bash

# in all cases run a script and print standard output and error to a file

./qa.int.py > qa.int.out 2>&1
./qa.int.error.py > qa.int.error.out 2>&1
./qa.one.quote.py > qa.one.quote.out 2>&1
./qa.py > qa.out 2>&1
./qa.zero.quote.py > qa.zero.quote.out 2>&1
./qb.error.py > qb.error.out 2>&1
./qb.py > qb.out 2>&1
