#!/usr/bin/python

"""
Learning journal unit 1, file qb.py
"""

a=1
"""
Declare a variable, initial value 1
"""
print 8%2
"""
Print remainder, 8 divided by 2, output 0
"""
print a
"""
Initial value of the variable, output 1
"""
a+=1
"""
Increment variable by one
"""
print a
"""
Print variable, output 2
"""
