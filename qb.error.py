#!/usr/bin/python

"""
Learning journal unit 1, file qb.error.py
"""

print "2"+2
"""
Traceback (most recent call last):
  File "./qb.error.py", line 7, in <module>
    print "2"+2
TypeError: cannot concatenate 'str' and 'int' objects

Try to concatenate string and integer, it is a syntax error.
"""
