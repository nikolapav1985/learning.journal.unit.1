#!/usr/bin/python

"""
Learning journal unit 1, file qa.int.py
"""

print (2++2)
"""
add positive 2 to 2, equals 4
"""
print (2--2)
"""
subtract negative 2 from 2, equals 4, 2--2=2+2=4
"""
print (2+-2)
"""
add negative 2 to 2, equals 0
"""
print (2-+2)
"""
subtract positive 2 from 2, equals 0
"""
print (02)
"""
print 2
"""
