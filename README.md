Learning journal unit 1
-----------------------

Scripts are based on exercise 1.1 (Downey 2015) of the guide and some of the additional examples.  

Included scripts
----------------

qa.int.py (integer arithmetic), qa.int.error.py (integer arithmetic error examples), qa.one.quote.py (string error example), qa.zero.quote.py (string error example), qa.py (string example), qb.py (more integer examples), qb.error.py (integer and string error examples).

Test environment
----------------

OS Lubuntu 16.04 LTS kernel version 4.13.0 (64 bit). Python version 2.7.12.

Test command
------------

Type ./run.sh to run all scripts and produce output. Output can also be found in comments of each script.

References
----------

Downey, A. (2015). Think Python: How to think like a computer scientist. Needham, Massachusetts: Green Tree Press.  
