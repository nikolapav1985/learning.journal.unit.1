#!/usr/bin/python

"""
Learning journal unit 1, file qa.one.quote.py
"""

print Hello"

"""
  
File "./qa.one.quote.py", line 7
    print Hello"
               ^
SyntaxError: EOL while scanning string literal

An EOL error means that Python hit the end of a line while going through a string. This can be because person forgot ending quotes, or because person tried to make a string extend past one line. Strings enclosed in single or double quotes cannot span multiple lines.

"""
